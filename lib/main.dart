import 'package:flutter/material.dart';


main() =>
    runApp(
      Directionality(
        textDirection: TextDirection.ltr,
        child: Container(
          color: Color(0xFFFFFFFF),
          child: Center(
            child: firstStWidget(),
          ),
        ),
      ),
    );


Image myImg = Image.network('https://forum.bits.media/uploads/downloaded/48_52d9e7d52ff87b8cf6eee6d43ecd60b6.gif',
    height: 150.0, width: 200.0);

class firstStWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Learn flutter'),
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter,
//                stops: [0.1, 0.5, 0.7, 0.9],
                colors: <Color>[
                  Colors.amber[800],
                  Colors.amber[500],
                  Colors.green[400],
                  Colors.amber[100],
                ])),
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 15.0),
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 25.0),
                      child: Image.network (
                        'https://upload.wikimedia.org/wikipedia/commons/1/17/Google-flutter-logo.png',
                        height: 120.0, width: 270.0,),),
                      Text('Iearn Flutter with us',
                        textDirection: TextDirection.ltr,
                        style: TextStyle(color: Color(0x775557FF),
                            fontStyle: FontStyle.italic,
                            fontSize: 27.0),),
                      Text('call: (095)-064-25-24',
                        textDirection: TextDirection.ltr,
                        style: TextStyle(color: Color(0xFF033000),
                            fontSize: 22.0),),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 25.0),
                child: Center(
                  child: Column(
                    verticalDirection: VerticalDirection.down,
                    children: <Widget>[
                      Text('It coast 1000', style: TextStyle(
                          fontSize: 25.0, fontStyle: FontStyle.italic),),
                      myImg,
                    ],
                  ),
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: MyFButton1(
//                        child: new Container(
////                          margin: EdgeInsets.only(left: 10.0, right: 5.0, bottom: 15.0),
//                          width: 200.0,
//                          height: 50.0,
//                          decoration: new BoxDecoration(
//                            color: Colors.orange,
//                            border: new Border.all(color: Colors.red, width: 3.0),
//                            borderRadius: new BorderRadius.circular(150.0),
//                          ),
//                          child: new Center(child: new Text('Flutter.io', style: new TextStyle(fontSize: 18.0, color: Colors.white),),),
//                        ),
                      ),
                    ),
                    Expanded(
                      child: MyFButton2(),
//                      child: FlatButton(
//                        child: new Container(
////                          margin: EdgeInsets.only(left: 5.0, right: 10.0, bottom: 15.0),
//                          width: 200.0,
//                          height: 50.0,
//                          decoration: new BoxDecoration(
//                            color: Colors.orange,
//                            border: new Border.all(color: Colors.red, width: 3.0),
//                            borderRadius: new BorderRadius.circular(150.0),
//                          ),
//                          child: new Center(child: new Text('m.habr.com', style: new TextStyle(fontSize: 18.0, color: Colors.white),),),
//                        ),
//                    )
                    ),
                  ],
                ),
//                child: GestureDetector(
//                  onTap: () {
//                  },
//                  child: ClipOval(
//                    child: Container(
//                      width: 150.0,
//                      height: 50.0,
//                      color: Colors.blue,
//                      child: Center(
//                        child: Text('flutter.io', style: TextStyle(
//                            color: Colors.white, fontSize: 25.0),),
//                      ),
//                    ),
//                  ),
//                ),
              ),
            ],
          ),
        ),
      ),
     );
  }
  }
  void _onT (BuildContext context){
    var alertDialog = new AlertDialog(
      title: Text('Button 1 pressed'), content: Image.network('https://www.chimpstickers.com/wp-content/uploads/2017/11/expression008-ok.png'));
    showDialog(context: context, builder: (BuildContext context){
      return alertDialog;
    });
  }


  class MyFButton1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: (){
          _onT(context);

        },
        child: FlatButton (

          child: new Container(
////                          margin: EdgeInsets.only(left: 5.0, right: 10.0, bottom: 15.0),
            width: 200.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: Colors.orange,
              border: new Border.all(color: Colors.red, width: 3.0),
              borderRadius: new BorderRadius.circular(150.0),
            ),
            child: new Center(child: new Text('flutter.io', style: new TextStyle(fontSize: 18.0, color: Colors.white),),),
          ),
        )
      )
    );
    // TODO: implement build
  }

  }

  class MyFButton2 extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return Container(
          child: FlatButton(
            child: new Container(

              ////                          margin: EdgeInsets.only(left: 5.0, right: 10.0, bottom: 15.0),
              width: 200.0,
              height: 50.0,
              decoration: new BoxDecoration(
                color: Colors.orange,
                border: new Border.all(color: Colors.red, width: 3.0),
                borderRadius: new BorderRadius.circular(150.0),
              ),
              child: new Center(child: new Text('m.habr.com',
                style: new TextStyle(fontSize: 18.0, color: Colors.white),),),
            ),
          )
      );
    }
  }